﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CalculateSubmergedVolume : MonoBehaviour {
	
    [Tooltip("Porcentaje de volumen sumergido")]
    public float _submergedVolume = 75;
    float _volume;
    MeshCollider _mC;

    void OnValidate()
    {
        Calculate();
    }

    void Start()
    {
        Calculate();
    }

    void Calculate()
    {
        _mC = GetComponent<MeshCollider>();
        _volume = ColliderExtensions.ComputeVolume(_mC) * _submergedVolume * 10;
        GetComponent<Rigidbody>().mass = _volume;
    }
}
