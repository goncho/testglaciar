﻿using UnityEngine;
using System.Collections;

public class DrawAxisGizmo : MonoBehaviour
{

    public bool onSelected = false;

    [Range(0.01f, 100f)]
    public float size = 0.1f;
    public Color colorX = Color.red * 0.7f;
    public Color colorY = Color.green * 0.7f;
    public Color colorZ = Color.blue * 0.7f;
    public float offsetZ;

    void Start()
    {
        //empty;
    }

    void OnDrawGizmos()
    {
        if (!onSelected)
        {
            Gizmos.color = colorX;
            Vector3 directionX = transform.TransformDirection(Vector3.right) * size;
            Gizmos.DrawRay(transform.position, directionX);

            Gizmos.color = colorY;
            Vector3 directionY = transform.TransformDirection(Vector3.up) * size;
            Gizmos.DrawRay(transform.position, directionY);

            Gizmos.color = colorZ;
            Vector3 directionZ = transform.TransformDirection(Vector3.forward) * size;
            Gizmos.DrawRay(transform.position + new Vector3(0, offsetZ, 0), directionZ);

            Gizmos.color = Color.gray * 0.25f;
            Gizmos.DrawRay(transform.position, -directionX);
            Gizmos.DrawRay(transform.position, -directionY);
            Gizmos.DrawRay(transform.position + new Vector3(0, offsetZ, 0), -directionZ);
        }
    }

    void OnDrawGizmosSelected()
    {
        if (onSelected)
        {
            Gizmos.color = colorX;
            Vector3 directionX = transform.TransformDirection(Vector3.right) * size;
            Gizmos.DrawRay(transform.position, directionX);

            Gizmos.color = colorY;
            Vector3 directionY = transform.TransformDirection(Vector3.up) * size;
            Gizmos.DrawRay(transform.position, directionY);

            Gizmos.color = colorZ;
            Vector3 directionZ = transform.TransformDirection(Vector3.forward) * size;
            Gizmos.DrawRay(transform.position + new Vector3(0, offsetZ, 0), directionZ);

            Gizmos.color = Color.gray * 0.25f;
            Gizmos.DrawRay(transform.position, -directionX);
            Gizmos.DrawRay(transform.position, -directionY);
            Gizmos.DrawRay(transform.position + new Vector3(0, offsetZ, 0), -directionZ);
        }
    }
}
