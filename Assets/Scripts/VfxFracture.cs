﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxFracture : MonoBehaviour {

    public bool _update;
    public float _sizeMaxThreshold;
    public Vector2 _sizeMinThreshold;
    [SerializeField]
    int numFractures;

    public Transform _fractureRoot;
    public GameObject _vfxIcePrefab;


    MeshFilter[] _meshFilters;
    public List<GameObject> _fractures;
    //List<ParticleSystem> _ps;
    public ParticleSystem[] _ps;

    void Start()
    {
        GetSize();

        for (int i = 0; i < _fractures.Count; i++)
        {
            GameObject gO = Instantiate(_vfxIcePrefab);
            gO.name = "VFX_Ice_" + i;
            gO.transform.parent = _fractures[i].transform;
            gO.transform.localPosition = Vector3.zero;
            gO.transform.localEulerAngles = Vector3.zero;

            Rigidbody rb = _fractures[i].AddComponent<Rigidbody>();
            _fractures[i].GetComponent<Rigidbody>().isKinematic = true;
            _fractures[i].GetComponent<Rigidbody>().useGravity = false;

            MeshCollider mC = _fractures[i].AddComponent<MeshCollider>();
            mC.sharedMesh = _fractures[i].GetComponent<MeshFilter>().mesh;
            mC.convex = true;
        }

        _ps = _fractureRoot.GetComponentsInChildren<ParticleSystem>();

        for (int i = 0; i < _ps.Length; i++)
        {
            var shape = _ps[i].shape;
            shape.meshRenderer = _ps[i].gameObject.GetComponentInParent<MeshRenderer>();
        }
    }

    void OnValidate()
    {
        if (_update)
        {
            GetSize();
            _update = false;
        }
    }

    void GetSize()
    {
        _fractures = new List<GameObject>();
        _fractures.Clear();

        _meshFilters = _fractureRoot.GetComponentsInChildren<MeshFilter>();

        for (int i = 0; i < _meshFilters.Length; i++)
        {
            Bounds bounds = _meshFilters[i].mesh.bounds;
            //float size = bounds.size.magnitude;
            float size = bounds.extents.x * bounds.extents.y * bounds.extents.z * 0.03f;

            if (size >= _sizeMaxThreshold)
            {
                _fractures.Add(_meshFilters[i].gameObject);
            }
            if (size >= _sizeMinThreshold.x && size <= _sizeMinThreshold.y)
            {
                _fractures.Add(_meshFilters[i].gameObject);
            }
        }

        numFractures = _fractures.Count;
    }
}
