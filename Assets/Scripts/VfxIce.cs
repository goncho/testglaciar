﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VfxIce : MonoBehaviour {

    public bool _update;
    public GameObject vfxFragmentsPrefab;
    public GameObject vfxIceStartPrefab;
    public GameObject vfxSplash;

    [SerializeField]
    public float _size;
    //[SerializeField]
    //float _size2;
    public float _splashSizeMultiplier = 1;
    public float _splashSpeedMultiplier = 100;
    public float _splashRateMultiplier = 1;
    [SerializeField]
    [Range(0,1)]
    public float _velocity;
    [SerializeField]
    [Range(0, 1)]
    float _velocityInverse;

    ParticleSystem[] _ps;
    Vector3 _previous;

    bool _activated = false;

    MeshCollider _meshCollider;

    ParticleSystem[] psSplashes;

    void Start()
    {
        GetSize();

        _ps = vfxFragmentsPrefab.GetComponentsInChildren<ParticleSystem>();
        psSplashes = vfxSplash.GetComponentsInChildren<ParticleSystem>();

        for (int i = 0; i < _ps.Length; i++)
        {
            var em = _ps[i].emission;
            //em.rateOverDistanceMultiplier = _size * 0.05f;
            em.rateOverDistanceMultiplier = _size * 0.25f;
        }

        _previous = transform.position;

        _meshCollider = GetComponent<MeshCollider>();
        _meshCollider.sharedMesh = GetComponentInParent<MeshFilter>().mesh;
    }

    void OnValidate()
    {
        if(_update)
        {
            GetSize();
            _update = false;
        }
    }

    void GetSize()
    {
        if (GetComponentInParent<MeshFilter>())
        {
            Bounds bounds = GetComponentInParent<MeshFilter>().mesh.bounds;
            //_size = bounds.size.magnitude;
            _size = bounds.extents.x * bounds.extents.y * bounds.extents.z * 0.03f;
            //_size2 = bounds.extents.x * bounds.extents.y * bounds.extents.z * 0.03f;
        }
    }

    void LateUpdate()
    {
        float currVelocity = ((transform.position - _previous).magnitude) / Time.deltaTime;
        _velocity = Mathf.Lerp(_velocity, currVelocity, 0.1f) * 0.1f;
        _velocity = Mathf.Clamp01(_velocity);
        _velocityInverse = Mathf.Abs(1 - _velocity);
        _previous = transform.position;
    }

    void Update()
    {
        if(!_activated)
        {
            if(_velocity > 0)
            {
                GameObject gO = Instantiate(vfxIceStartPrefab);
                gO.name = "VFX_Ice_Start";
                gO.transform.parent = transform;
                gO.transform.localPosition = Vector3.zero;
                gO.transform.localEulerAngles = Vector3.zero;

                ParticleSystem ps = gO.GetComponent<ParticleSystem>();
                var shape = ps.shape;
                shape.meshRenderer = GetComponentInParent<MeshRenderer>();
                ps.Play();

                _activated = true;
            }
        }        
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag == "TriggerWater" && _velocity > 0)
    //    {
    //        vfxSplash.transform.position = new Vector3(transform.position.x, 3, transform.position.z);
    //        vfxSplash.transform.eulerAngles = Vector3.right * -90;
    //        vfxSplash.transform.localScale = Vector3.one * _size * _splashSizeMultiplier;

    //        //psSplashes = vfxSplash.GetComponentsInChildren<ParticleSystem>();

    //        for (int i = 0; i < psSplashes.Length; i++)
    //        {
    //            psSplashes[i].Play();
    //        }
    //    }
    //}

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "TriggerWater" && _velocity > 0)
        {
            vfxSplash.transform.position = new Vector3(transform.position.x, 3, transform.position.z);
            vfxSplash.transform.eulerAngles = Vector3.right * -90;
            vfxSplash.transform.localScale = Vector3.one * _size * _splashSizeMultiplier;

            //psSplashes = vfxSplash.GetComponentsInChildren<ParticleSystem>();

            for (int i = 0; i < psSplashes.Length; i++)
            {
                var main = psSplashes[i].main;
                main.startSpeedMultiplier = _velocity * _splashSpeedMultiplier;
                var em = psSplashes[i].emission;
                em.rateOverTimeMultiplier = _velocity * _size * _splashRateMultiplier;

                psSplashes[i].Play();
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "TriggerWater")
        {
            //psSplashes = vfxSplash.GetComponentsInChildren<ParticleSystem>();

            for (int i = 0; i < psSplashes.Length; i++)
            {
                psSplashes[i].Stop();
            }
        }
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if(other.gameObject.tag == "TriggerWater" && _velocity > 0)
    //    {
    //        GameObject gO = Instantiate(vfxSplashPrefab);
    //        gO.name = "VFX_Splash";
    //        gO.transform.position = new Vector3(transform.position.x, 3, transform.position.z);
    //        gO.transform.eulerAngles = Vector3.right * -90;
    //        gO.transform.localScale = Vector3.one * _size * _splashSizeMultiplier;

    //        ParticleSystem[] ps = gO.GetComponentsInChildren<ParticleSystem>();

    //        for (int i = 0; i < ps.Length; i++)
    //        {
    //            var main = ps[i].main;
    //            main.startSpeedMultiplier = _velocity * _splashSpeedMultiplier;
    //        }

    //        Destroy(gO, 12);
    //    }
    //}
}
