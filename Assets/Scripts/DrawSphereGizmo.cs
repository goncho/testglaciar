﻿using UnityEngine;
using System.Collections;

public class DrawSphereGizmo : MonoBehaviour {

    public bool onSelected = false;
    public bool solid = false;

    [Range(0.0001f, 10)]public float size = 1f;
	public Color color = Color.white;

    void OnDrawGizmos()
	{
        if(!onSelected)
        {
            Gizmos.color = color;
            if(solid)
            {
                Gizmos.DrawSphere(transform.position, size);
            }
            else
            {
                Gizmos.DrawWireSphere(transform.position, size);
            }            
        }
    }

    void OnDrawGizmosSelected()
    {
        //if (onSelected)
        //{
        //    Gizmos.color = color;
        //    if (solid)
        //    {
        //        Gizmos.DrawSphere(transform.position, size);
        //    }
        //    else
        //    {
        //        Gizmos.DrawWireSphere(transform.position, size);
        //    }
        //}

        Gizmos.color = color;
        Gizmos.DrawSphere(transform.position, size * 2);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, size * 2);
    }
}
