﻿using System.Collections;
using UnityEngine;
//using UnityEngine.UI;

namespace PlayWay.Water.Samples
{
	public class PresetDropdownDani : MonoBehaviour
	{
		[SerializeField]
		private Water water;

		//[SerializeField]
		//private WaterProfile[] profiles;

        public WaterProfile sourceProfile;
        public WaterProfile targetProfile;

        public float _startTime;
        public float _changeTime;
        public AnimationCurve _changeCurve;
        [Range(0, 1)]
        public float _multiplier = 1;
        bool _changeProfile;
        [Range(0, 1)]
        public float t;
        //[Range(0, 1)]
        //public float _test;
        bool _activated;

		//void Start()
		//{
  //          Invoke("ChangeProfile", _startTime);
		//}

        void ChangeProfile()
        {
            Debug.Log("ChangeProfile");
            StartCoroutine(ChangeProfileRutina());
        }

        void Update()
        {
            //if(Mathf.Approximately(Time.timeSinceLevelLoad, _startTime)) StartCoroutine(ChangeProfileRutina());

            //t = t * _multiplier;

            water.SetProfiles(
                    new Water.WeightedProfile(sourceProfile, 1.0f - t),
                    new Water.WeightedProfile(targetProfile, t)
                );

            if (Time.timeSinceLevelLoad >= _startTime && !_activated)
            {                
                ChangeProfile();
                _activated = true;
            }
                


            //water.SetProfiles(
            //       new Water.WeightedProfile(sourceProfile, 1 - _test),
            //       new Water.WeightedProfile(targetProfile, _test)
            //   );
        }

        IEnumerator ChangeProfileRutina()
        {
            Debug.Log("ChangeProfileRutina");

            float timer = 0.0f;
            while (timer <= _changeTime)
            {
                t = _changeCurve.Evaluate(timer / _changeTime) * _multiplier;

                //_matEye.SetColor("_EmissionColor", Color.Lerp(Color.black, Color.white * _emission, t));

                //water.SetProfiles(
                //    new Water.WeightedProfile(sourceProfile, 1.0f - t),
                //    new Water.WeightedProfile(targetProfile, t)
                //);

                timer += Time.deltaTime;

                yield return null;
            }
        }
	}
}
