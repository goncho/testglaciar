﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caustics : MonoBehaviour
{
	[SerializeField]
	private int _framesPerSecond = 30;
	[SerializeField]
	private Texture[] _textures = null;
	[SerializeField]
	private Vector3 _direction = Vector3.up;

	private int i = 0;
	private float _time = 0.0f;
	private float _value = 0.0f;

	private void Start()
	{
		Shader.SetGlobalTexture("_Caustics1", _textures[i]);
		Shader.SetGlobalTexture("_Caustics2", _textures[(i + 1 >= _textures.Length) ? 0 : i + 1]);
	}

	private void Update()
	{
		if (_textures != null && _textures.Length >= 1)
		{
			_time += Time.deltaTime;
			float percentage = _time * _framesPerSecond;
			_value = Mathf.Lerp(0.0f, 1.0f, percentage);

			if (_value >= 1.0f)
			{
				_time = 0.0f;
				i = i + 1 >= _textures.Length ? 0 : i + 1;
				_value = 0.0f;
				Shader.SetGlobalTexture("_Caustics1", _textures[i]);
				Shader.SetGlobalTexture("_Caustics2", _textures[(i + 1 >= _textures.Length) ? 0 : i + 1]);
			}
            Shader.SetGlobalFloat("_CausticsLerp", _value);

        }

		Quaternion direction = Quaternion.LookRotation(_direction, new Vector3(_direction.z, _direction.x, _direction.y));
        Matrix4x4 causticsMatrix = Matrix4x4.TRS(new Vector3(0.0f, 0.0f, 0.0f), direction, Vector3.one);
		Shader.SetGlobalMatrix("_CausticsRotation", causticsMatrix);
	}
}
