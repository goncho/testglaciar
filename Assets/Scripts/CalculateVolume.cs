﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CalculateVolume : MonoBehaviour {
    
    public float _volume;
    MeshCollider _mC;

    void OnValidate()
    {
        Calculate();
    }

    //void Start()
    //{
    //    Calculate();
    //}

    void Calculate()
    {
        _mC = GetComponent<MeshCollider>();
        _volume = ColliderExtensions.ComputeVolume(_mC) * 10;
        
    }
}

