﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Blackfire Studio/Ice"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_SpecColor("Specular", Color) = (0.2,0.2,0.2)
		[NoScaleOffset]
		_SpecGlossMap("Specular (RGB) Smoothness (A)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0.0, 1.0)) = 0.5

		[Header(Normal)]
		[NoScaleOffset][Normal]
		_BumpMap("Normal Map", 2D) = "bump" {}
		_BumpScale("Scale", Float) = 1.0
		[NoScaleOffset]
		_DetailBumpMap("Detail Normal Map", 2D) = "bump" {}
		_DetailBumpMapScale("Detail Scale", Float) = 1.0
		_DetailBumpScale("Detail Normal Scale", Float) = 1.0

		[Header(Occlusion)]
		[NoScaleOffset]
		_OcclusionMap("Occlusion", 2D) = "white" {}
		_OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
		_DetailOcclusionMap("Detail Occlusion", 2D) = "white" {}
		_DetailOcclusionStrength("Detail Occlusion Strength", Range(0.0, 1.0)) = 1.0
		_DetailOcclusionScale("Detail Occlusion Scale", Float) = 1.0

		[Header(Translucency)]
		[NoScaleOffset]
		_ThicknessMap("Thickness", 2D) = "black" {}
		_ThicknessRange("Range", Range(0.0, 1.0)) = 1.0
		[NoScaleOffset]
		_ThicknessDetailMap("Thickness Detail", 2D) = "black" {}
		_ThicknessLUT("Thickness LUT", 2D) = "white" {}
		_ThicknessColor("Color", Color) = (0.2,0.2,0.2)
		_ThicknessGeneral("General", Float) = 1.0
		_ThicknessStrength("Strength", Float) = 1.0
		_ThicknessDistortion("Distortion", Range(0, 0.5)) = 0.1
		_ThicknessPower("Power", Range(1.0, 15.0)) = 4.0
		_ThicknessScale("Scale", Range(0.0, 10.0)) = 2.0

		//_ShadowColor("Shadow Color", Color) = (0.2,0.2,0.2)

		[Header(Caustics)]
		[Toggle(ENABLE_CAUSTICS)] _CausticsEnable("Enable", Float) = 0
		_CausticsColor("Color", Color) = (1.0, 1.0, 1.0)
		_CausticsStrength("Strength", Float) = 1.0
		_CausticsMaskLevel("Mask Level", Float) = 1.0
		_CausticsSmoothness("Mask Smoothness", Float) = 1.0
		_CausticsScale("Scale", Float) = 1.0
		_CausticsSpeed("Speed", Float) = 1.0
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 300
		
		CGPROGRAM
		#pragma surface surf Ice fullforwardshadows addshadow
		#pragma target 3.0
		#pragma multi_compile __ ENABLE_CAUSTICS
		#include "UnityPBSLighting.cginc"

		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
			float3 localPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		fixed4		_Color;
		sampler2D	_MainTex;
		//half3		_SpecColor;
		sampler2D	_SpecGlossMap;
		half		_Glossiness;
		sampler2D	_BumpMap;
		half		_BumpScale;
		sampler2D	_OcclusionMap;
		half		_OcclusionStrength;

		sampler2D	_ThicknessMap;
		sampler2D	_ThicknessDetailMap;
		sampler2D	_ThicknessLUT;
		half3		_ThicknessColor;
		half		_ThicknessStrength;
		half		_ThicknessDistortion;
		half		_ThicknessPower;
		half		_ThicknessScale;
		half		_ThicknessGeneral;
		half		_ThicknessRange;

		//half3		_ShadowColor;

		sampler2D	_DetailBumpMap;
		half		_DetailBumpMapScale;
		half		_DetailBumpScale;

		sampler2D	_DetailOcclusionMap;
		half		_DetailOcclusionStrength;
		half		_DetailOcclusionScale;

		half		_CausticsMaskLevel;
		half		_CausticsSmoothness;
		half		_CausticsScale;
		half		_CausticsSpeed;
		half		_CausticsStrength;
		half3		_CausticsColor;

		uniform sampler2D	_Caustics1;
		uniform sampler2D	_Caustics2;
		uniform half		_CausticsLerp;
		uniform float4x4	_CausticsRotation;

		struct SurfaceOutputIce
		{
			fixed3 Albedo;
			fixed3 Specular;
			fixed3 Normal;
			half3 Emission;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
			fixed2 Thickness;
		};

		void vert(inout appdata_full v, out Input o)
		{
			UNITY_INITIALIZE_OUTPUT(Input, o);
			o.localPos = v.vertex.xyz;
		}

		//float3 LinearDodge(float3 a, float3 b) { return (a + b); }

		float3 Overlay(float3 a, float3 b)
		{
			float3 r = float3(0, 0, 0);
			if (a.r > 0.5) { r.r = 1 - (1 - 2 * (a.r - 0.5))*(1 - b.r); }
			else { r.r = (2 * a.r)*b.r; }
			if (a.g > 0.5) { r.g = 1 - (1 - 2 * (a.g - 0.5))*(1 - b.g); }
			else { r.g = (2 * a.g)*b.g; }
			if (a.b > 0.5) { r.b = 1 - (1 - 2 * (a.b - 0.5))*(1 - b.b); }
			else { r.b = (2 * a.b)*b.b; }
			return r;
		}

		inline half4 LightingIce(SurfaceOutputIce s, half3 viewDir, UnityGI gi)
		{
			s.Normal = normalize(s.Normal);

			half oneMinusReflectivity;
			s.Albedo = EnergyConservationBetweenDiffuseAndSpecular(s.Albedo, s.Specular, /*out*/ oneMinusReflectivity);

			half outputAlpha;
			s.Albedo = PreMultiplyAlpha(s.Albedo, s.Alpha, oneMinusReflectivity, /*out*/ outputAlpha);

			half4 c = UNITY_BRDF_PBS(s.Albedo, s.Specular, oneMinusReflectivity, s.Smoothness, s.Normal, viewDir, gi.light, gi.indirect);

			// Translucency - Based on Dice's model
			half3 l = gi.light.dir + s.Normal * _ThicknessDistortion;
			//float indotl = saturate(dot(s.Normal, -l));
			float vdotl = saturate(dot(viewDir, -l));
			vdotl = pow(vdotl, _ThicknessPower) * _ThicknessScale;

			// Pass 1
			half thickness1 = vdotl * (s.Thickness.r + s.Thickness.g);
			half3 translucency1 = tex2D(_ThicknessLUT, half2(thickness1, 0.0));
			translucency1 = translucency1 * gi.light.color *_ThicknessColor.rgb * _ThicknessStrength;
			//translucency1 = saturate(translucency1);

			// Pass 2
			half thickness2 = vdotl;
			half3 translucency2 = tex2D(_ThicknessLUT, half2(thickness2, 0.0));
			translucency2 = translucency2 * gi.light.color *_ThicknessColor.rgb * _ThicknessGeneral;
			//translucency2 = saturate(translucency2);

			//indotl = pow(indotl, _ThicknessPower) * _ThicknessScale;
			//half thickness2 = vdotl;

			// TODOOOOOOOOOO
			c.rgb = lerp(c.rgb, Overlay(c.rgb, translucency1), pow(s.Thickness.r, 6)) + translucency2 * pow(1 - s.Thickness.r, 1) * 0.35;
			c.a = outputAlpha;

			// Debug
			//c.rgb = indotl;

			return c;
		}

		inline void surf(Input IN, inout SurfaceOutputIce o)
		{
			// Default
			half2 uv = IN.uv_MainTex;
			half2 uvNormal = uv * _DetailBumpScale;
			half2 uvOcclusion = uv * _DetailOcclusionScale;

			// Albedo
			half3 albedo = _Color.rgb * tex2D(_MainTex, uv).rgb;

			// Occlusion
			half occlusion = LerpOneTo(tex2D(_OcclusionMap, uv).g, _OcclusionStrength);
			half detailOcclusion = LerpOneTo(tex2D(_DetailOcclusionMap, uvOcclusion).g, _DetailOcclusionStrength);
			occlusion *= detailOcclusion;

			// Normal
			half3 normalTangent = UnpackScaleNormal(tex2D(_BumpMap, uv), _BumpScale);
			half3 detailNormalTangent = UnpackScaleNormal(tex2D(_DetailBumpMap, uvNormal), _DetailBumpMapScale);
			half3 normal = BlendNormals(normalTangent, detailNormalTangent);

			// Specular
			half4 specularGlossiness = tex2D(_SpecGlossMap, uv);
			specularGlossiness.rgb *= _SpecColor.rgb;
			specularGlossiness.a *= _Glossiness;

			// Thickness
			half2 thickness = half2(1.0 - tex2D(_ThicknessMap, uv).r,
									1.0 - tex2D(_ThicknessDetailMap, uv).r);
			thickness.r *= _ThicknessRange;

			// Caustics
#ifdef ENABLE_CAUSTICS
			half mask = dot(IN.worldPos, half3(0.0, 1.0, 0.0));
			mask = smoothstep(_CausticsMaskLevel - _CausticsSmoothness, _CausticsMaskLevel + _CausticsSmoothness, mask);
			mask = 1.0 - mask;

			float3 speed = _CausticsSpeed * _Time.y;
			half scale = _CausticsScale;
			float3 worldCoord = (IN.worldPos + speed) / scale;
			float2 coords = mul(float4(worldCoord, 0), _CausticsRotation).xy;

			half3 caustics1 = tex2D(_Caustics1, coords);
			half3 caustics2 = tex2D(_Caustics2, coords);
			half3 caustics = lerp(caustics1, caustics2, _CausticsLerp) * mask;
			caustics = saturate(caustics * _CausticsStrength * _CausticsColor);
#else
			half3 caustics = half3(0.0, 0.0, 0.0);
#endif

			o.Albedo = albedo;
			o.Specular = specularGlossiness.rgb;
			o.Smoothness = specularGlossiness.a;
			o.Normal = normal;
			o.Occlusion = occlusion;
			o.Thickness = thickness;
			o.Emission = caustics;

			o.Alpha = 1.0;

			// Debug
			//o.Albedo = caustics;
		}

		inline void LightingIce_GI(SurfaceOutputIce s, UnityGIInput data, inout UnityGI gi)
		{
		#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
			gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
		#else
			Unity_GlossyEnvironmentData g = UnityGlossyEnvironmentSetup(s.Smoothness, data.worldViewDir, s.Normal, s.Specular);
			gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal, g);
		#endif
		}
		ENDCG
	}
	FallBack "Diffuse"
}
