﻿using UnityEngine;
using System.Collections;

public class Vfx_ShoreLineProjector : MonoBehaviour {

    Material _material;
    //SceneManager _sceneManager;
    public float _fog;
    public Color _colorA = Color.white;
    public Color _colorB = Color.black;
    int _sceneSetNumber;

    void Start()
    {
        _material = GetComponent<Projector>().material;
        GameObject _gameController = GameObject.FindGameObjectWithTag("GameController");
        //_sceneManager = _gameController.GetComponent<SceneManager>();
        //_sceneSetNumber = _sceneManager.sceneSetNumber;
    }

    void LateUpdate()
    {
        //_fog = _sceneManager._fog;
        //Color _color = Color.Lerp(_colorA, _colorB, _fog * _sceneManager.sceneSets[_sceneSetNumber]._ShoreLineProjectorMultiplier);
        //_material.SetColor("_TintColor", _color);
    }
}
